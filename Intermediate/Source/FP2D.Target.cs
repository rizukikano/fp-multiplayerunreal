using UnrealBuildTool;

public class FP2DTarget : TargetRules
{
	public FP2DTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("FP2D");
	}
}
